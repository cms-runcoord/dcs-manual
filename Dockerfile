# ENVIRONMENT
FROM nginx:latest
RUN apt-get update
RUN apt-get install -y python3 python3-pip

# COPY
RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app

# INSTALL PYTHON
RUN pip install -r requirements.txt

# BUILD
RUN mkdocs build

# RELEASE
RUN mkdir -p /usr/share/nginx/html && \
cp -r /usr/src/app/site/* /usr/share/nginx/html/ && \
cp /usr/src/app/nginx/nginx.conf /etc/nginx/nginx.conf && \
touch /var/run/nginx.pid

# SUPPORT RUNNING AS ARBITRARY USER WHICH BELONGS TO THE ROOT GROUP
RUN chmod g+rwx /var/cache/nginx /var/run/nginx.pid /var/log/nginx && chmod -R g+w /etc/nginx

# EXPOSE
EXPOSE 8080
