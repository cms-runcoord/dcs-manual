## Personal Protective Equipment (PPE) - KIOSK

A person going underground is required to wear the appropriate PPE consisting of:

1. Helmet with functioning lamp.

2. Safety shoes.

3. Self-Rescue mask (for the LHC tunnel and UXC55 X6 level, and X0 or X5 levels in UXC55 during short technical stops).

4. Optionally, work specific PPE (harness, eye/hearing protection, gloves, etc.).

!!! note
     * The shifter has the authority to deny access to personnel without appropriate PPE.


### Lending of PPE/Equipment
It is possible that somebody going underground will ask for PPE (helmet, safety shoes, gas portable sensors, lamps,
operational dosimeters, self-rescue masks, etc.).

- Use the “kiosk” touch screen and the bar-code scanner available on your desk.
- Alternatively (in case of issues with the “kiosk”), please make an Elog entry with the name of the person when lending PPE.

If the Kiosk isn’t working properly:

- Try to switch it off and switch it back on;
- Login in with the “Log in with Kerberos” (in case do it twice), or with “saftour” account (for both the log in required) credentials to be found in the “Blue password box” under “K”.
- If it's still not working please report to 165000 and in your Elog.

If the Kiosk show the message “No part selected” it means that the equipment was not returned, so return it and then you will be able to take it without problems.


### Operational Dosimeters

#### Case 1
The operational dosimeter (DMC) was not properly “switched off” and displays a dose (i.e. 0.000) instead of displaying “CMS”. 

**<span style="color: blue;"> Actions to take:</span>**

**<span style="color: blue;">1. Place the dosimeter on the reader (on your desk or in SDX5) and it will be “switched off”. </span>**

**<span style="color: blue;">2. Write this down in your Elog with the dosimeter ID.</span>**


#### Case 2
The operational dosimeter displays “Low battery” and is beeping regularly.

**<span style="color: blue;"> Actions to take:</span>**

**<span style="color: blue;">1. Place the dosimeter in the locker “Am 95” (3562/R-010 - code to be found in the Blue “password box” under “L”).</span>**

**<span style="color: blue;">2. Send an email to the CMS-RSO (cms-rso@cern.ch).</span>**



#### Case 3
The operational dosimeter displays nothing (the screen is blank - in this case the battery is dead).

**<span style="color: blue;"> Actions to take:</span>**

**<span style="color: blue;">1. Place the dosimeter in the locker “Am 95” (3562/R-010 - code to be found in the Blue “password box” under “L”).</span>**

**<span style="color: blue;">2. Send an email to the CMS-RSO (cms-rso@cern.ch).</span>**



#### Case 4

The worker cannot return the operational dosimeter which displays “Low battery” (it prevents him/her to switch on another operational dosimeter)

**<span style="color: blue;"> Actions to take:</span>**

**<span style="color: blue;">1. Call the Dosimetry Service 72301 or 72155 and communicate the person ID</span>**

















