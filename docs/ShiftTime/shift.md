## Shift Time

Once you have all the Technical Shifter training requirements, you should:

- Send an email to the Shift Organization Team **<span style="color: blue;">"cms-sldcs-shiftsupport@cern.ch"</span>** in order to book your first Shadow Shift **specifying the days and times you prefer**
	
	!!! note
             * Shadow shifts are NOT allowed during nights or weekends.
             
       

###Shadow Shifts
At the begining of your Shadow Shift is very important to verify that you have all permissions in order to take control over the system. To do this, you should place your ID Card in the card reader
!!! warning
     * In case the system does not identify your user, you must call the Central DCS (16 2229). 
     
     * Indicate that your badge does not synchronize well in the egroup of the shiftlist operator. In the meantime you can ask the shift leader to place the card while DCS expert fixes the problem.
     
     
Follow the checklist given to the Main Technical Shifter.


### Shift Tool 

After having your Shadow Shift you would be able  to select your own shifts as Main Shifter through the following site:
 
[ShiftTool](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/shiftlist)

![Shift Tool](Shift-tool1.png)

Select the Month, System and Shift Type, then click on "Show" button
![Select](select.png)

In section "Shift Selection", you'll see all available slots for Main Shifter (Shift Flavor). You can select the one that suits you best
![Select Shift](selectShift.png)

### Begining of the Main Shift

Make sure you check over all next points and write an ELOG with the information

	- MACHINE MODE: PROTON PHYSICS
	- BEAM MODE:    NO BEAM
	- MAGNET:       ON (18164.0 A / 3.8 T / 4509 l / 4.9 K)
	- CMS:          NOT READY
	- Subdetectors Status: (CENTRAL/LOCAL, ON/OFF/STANDBY)
		- PIXEL:              IN CENTRAL  / OFF-LOCKED
		- STRIPS:             IN LOCAL    / OFF-LOCKED
		- ECAL:               IN CENTRAL  / ON
		- HCAL:               IN CENTRAL  / ON
		- DT:                 IN CENTRAL  / STANDBY
		- RPC:                IN CENTRAL  / OFF
		- CSC:                IN CENTRAL  / ON
		- GEM:                IN CENTRAL  / NOT_READY
	- RACKS: # ON;  # OFF;  # error;  # DSS (Rack numbers)
	- COOLING:                OK
	- DCS screens:            OK
	- Check-List:
		1. DSS PANEL:         OK (Describe)
		2. TETRA Radios:      8
		3. Access screens:    OK
		4. Access sheets:     OK 
		5. Intercom:          OK
		6. CCTV Video:        OK 
		7. DSS screens:       OK 
		8. REMUS:             OK
		9. ODH sensors:       4 
		10. DMC:              55
		11. Lamps:            2 
		12. PPE (helmets):    1 
		13. GSM Phones:       5 
		14. Traka key box:    OK
			- Traka key #1 : missing key number 
			- Traka ley #2 : Missing key number
		15. Unimux boxes:     OK
		16. Desk phones:      OK
		17. Check-out Kiosk:  OK
		
		
###### DSS Panel
Check that there is no obstruction in the access of the DSS Panel (chairs, bags, storage, etc.) Verify that no key is inserted in any of the keyhole (in particular "Enabled DSS action").
![DSS Panel](panel.jpeg)

###### TETRA Radios
Check that all 8 TETRA radios are in place, 16 batteries charging (green or orange led ON). <span style="color: blue;">If not: write in the elog and contact CMS TC (16 5000). </span>
![TETRA Radios](tetra.jpeg)

###### Access Screens
Check that the three access screens are intact. <span style="color: blue;">If not: consult "How to use the new Access Panel" on the Desk (EDMS 12 33310). </span>.

Check that there's no "ghost" in the caverns. Check the manual for further guidance.
<span style="color: blue;">If not: contact the "ghost" by phone and email. Make an entry in the elog. Inform 16 5000. </span>

###### Access Registration sheets
Check that Empty "Access registration" sheets are available close to the access screens. <span style="color: blue;">If not: print some sheets (EDMS 12333110). </span>
![Access Sheets](undg3.png)

###### Intercom 
Check that the three intercoms are working: "Public Address" (God's voice), PAD intercom and SCX5 entrance: call one of the undergroundPAD and then send a vocal message through the "Public Addres". <span style="color: blue;">If not: write in the elog and contact 165000 for the 1st one and 72201 for the 2 others. </span>

###### CCTV Video monitoring
Check that the 4 screens for video monitoring are displaying: UXC55 on 1 screen, SCX5 first-floor on a 2nd screen, visitor path (USC55, lift, SDX5, etc.) on a 3rd screen, and miscellaneous on a 4th one. <span style="color: blue;">If not: Contact Zoltari Szillasi 16 5114 .</span>
![Access Screens](udgd1.png)

###### DSS and Alarm Screens
Check that all 6 screens are ON and displaying the DSS (2 screens), Sniffer, CSAM, Safety Panel/GerdSystems, Tetra/ILDA.
![DSS Alarms](DSSalarmScreen.png)

Wake up the Sniffer and check for absence of alarms. Follow the alarm handling sheet if needed.
![Sniffer](snifer.jpeg)

###### REMUS, Dosimetry
Check that the REMUS screen is ON when moving the mouse and that the UXC55 is displayed. <span style="color: blue;">If not, try to solve it or contact cms-rso@cern.ch .</span>
Check that the LDM3000 (DMC) and the IMPACT app. are active.

###### ODH/CO2/Gas Sensors
Check that the 4 sensors are in their slots and on charge (red led ON or blinking) <span style="color: blue;">If not: check the elogs or contact 16 5000 .</span>
![Gas sensors](gasSensors.jpeg)

###### Operational Dosimeters DMC and kiosk
Check that all 33 operational dosimeters are present and display "CMS". <span style="color: blue;">If not: check the elogs or contact cms-rso@cern.ch .</span>
Check that the kiosk is operational. <span style="color: blue;">If not: Please contact the person in charge .</span>
![Operational Dosimeters](dosimeters.jpeg)

###### Lamps
Check that the 2 lamps in the kiosk are working properly <span style="color: blue;">If not: Check the elogs or contact cms-rso@cern.ch .</span>
![Lamps](lamps.jpeg)

###### PPE
Check that the Technical shifter's helmet is availableand that the head lamp is working. <span style="color: blue;">If not: write in your elog or replace the batteries if needed (spare batteries in the red safe) .</span>

###### GSM Phones
Check that the 5 GSM mobile phones are in the box, properly charged and ON, with Swisscom network.
![GSM Phones](GSMphones.jpeg)

###### "Traka key cabinets"
Check that the 2 "traka" key cabinets are working by badging with your CERN cardon the card reader. <span style="color: blue;">If not: contact Zoltan Sxillasi 16 5114 or Frank Glege 16 3913.</span>
![Traka Cabinets and list of Keys](Traka1.jpeg)
![Traka Cabinets](Traka2.jpeg)


###### Unimux boxes
Check that the 2 black boxes KVM for mouse and keyborad are working. <span style="color: blue;">If not: write an elog and contact 16 5000.</span>
![Black Box](blackBox.jpeg)





### During your Shift
You have to write entries in the ELOG of the main activities that occurred during the shift.









