## Biometry Bypass

**<span style="color: red;">Attention</span>**

The token distribution is only possible under the following conditions:

 1. Explicit derogation given by the TC on-call 165000.
 
 2. Registration of the name of the person and the token number in the CMS Online Elog.


When the person is in front of the PAD:

1. Dial the appropriate number on the intercom in order to start vocal communication (press X to stop it).
2. Turn the key inside the dedicated key slot (PM54, UP55 or UPX56).
3. Make sure the Biometry Bypass field is visible on the Access System Screen.
4. Ask the person to badge with the token.
5. Keep the key turned all over the process of passing through the PAD.
6. Release the key only once the person has passed through the PAD successfully.


Once the person has entered:

- Check that the “visitor token” is visible on the list of people registered by the system.

In case of any problem (impossibility to pass through the PAD after several attempts, biometry bypass impossible, token not registered in the system, etc.):

- Immediately inform the TC on-call 165000 and report in your Elog.


