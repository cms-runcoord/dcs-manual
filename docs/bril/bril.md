## BRIL Subsystems

### BRIL - PLT detector monitoring
 In the event of any problems or alarms, please call the BRIL DOC (165501) first. You can also call the [PLT](https://twiki.cern.ch/twiki/bin/view/CMS/PLT) DOC (167646) if the BRIL DOC is not reachable for whatever reason, but the BRIL DOC should be the first point of contact. 
 
 In general please call the BRIL DOC if it is necessary to turn the PLT ON or OFF. In the event of an urgent situation the DCS shifter can also turn OFF the PLT themselves, but do not turn the PLT back ON without consulting with the DOC (if unable to do so for any reason, just leave the PLT OFF).

When switching PLT OFF, some alerts may be triggered due to the fast ramp down speed used. As long as the alerts are gone within a short space of time (within a few seconds) they may be acknowledged in the alert screen.

Please also call the BRIL DOC (165501) in the event of any humidity or cooling problems in the tracker volume. 

### BRIL - BCML HV monitoring
 One can use the DIP browser to monitor the "live" values for the number of HV channels in BCM2 that are ON. The instructions are as follows

- Login to cmsusr0 (your linux machine> ssh -X yourUserName@cmsusr0)
- Setup the proper variables (cmsusr0> source ~brmshift/.bashrc)
- Launch the browser (cmsusr0> ~brmshift/bin/dipBrowser.sh)
- Click the >> in the upper right corner of the dip browser window.
- Click on the key symbol next to "dip", then CMS, then BRM then BCM2Summary
- Select "Status" and the data window should appear.
- **<span style="color: red;">The HV_CHANNELS_ON should read 32</span>**.
- Check that the time is updating at the bottom of the window to verify that the data is "live". 

NOTE: DIP is also available (live) via WBM, under Core Services. It does not work on all browsers or on all OSs. Select dip > CMS > BCM2Summary. 






