## Checklists 

* [Begin of Shift Checklist](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/checklist_centralDCS.pdf): checklist to be completed in the beginning of the shift;

* Beam Injection checklist: to be completed before declaring CMS Ready for beam injection;
        excel file. 

* Data-taking Checklist: checklist to be completed every two hours within a shift (template to be defined); 

* Shift Summary Checklist: to be completed at the end of each shift (template to be defined). 





