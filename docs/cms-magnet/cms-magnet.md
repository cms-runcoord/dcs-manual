## CMS Magnet Ramp: subdetectors' states 

###If magnet is already ramping... 

**<span style="color: red;">DO NOT TOUCH DCS STATUS OF ANY SUBSYSTEM</span>**

### Before magnet ramp (either ramp up or ramp down), get ready... 

**<span style="color: red;">DO NOT TURN ALL SYSTEMS OFF!</span>**

 After you receive a call from Technical Coordination saying they want to ramp the magnet, do the following THREE steps to be READY FOR MAGNET RAMP. You should be done within **15 minutes**.

**Step 1**: DAQ shifter stop the run.

**Step 2**: Do not wait for the DAQ shifter to finish in Step 1. DCS shifter does the following for each partition. **<span style="color: red;">DO NOT TURN ALL SYSTEMS OFF!</span>** If you cannot do the action (because the subsystem is in DCS local), call the DOC. If you cannot reach the DOC or DCS contact, DO NOT PANIC. In any case, AUTOMATIC action will occur. 

* **BPIX: do nothing** --> the detector is safe
	
* **FPIX: do nothing** --> the detector is safe

* **TIB/TID: do nothing** --> the detector is safe
	
* **TOB: do nothing** --> the detector is safe
	
* **TEC+: do nothing** --> the detector is safe
	
* **TEC-: do nothing** --> the detector is safe
	
* **EE-: call the DOC**. If you cannot reach the DOC, call the [ECAL DCS contact](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBContactListDetails1#EcAl) --> the detector is safe 
		
* **EB-: call the DOC**. If you cannot reach the DOC, call the [ECAL DCS contact](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBContactListDetails1#EcAl) --> the detector is safe
		
* **EB+: call the DOC**. If you cannot reach the DOC, call the [ECAL DCS contact](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBContactListDetails1#EcAl) --> the detector is safe
		
* **EE+: call the DOC**. If you cannot reach the DOC, call the [ECAL DCS contact](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBContactListDetails1#EcAl) --> the detector is safe 
		
* **ES-: do nothing** --> the detector is safe
	
* **ES+: do nothing** --> the detector is safe
	
* **HCAL: do nothing** --> the detector is safe
	
* **DT0: go to STANDBY, then go to DCS local** --> the detector is safe in STANDBY and DCS local
	
* **DT-: go to STANDBY, then go to DCS local** --> the detector is safe in STANDBY and DCS local
	
* **DT+: go to STANDBY, then go to DCS local** --> the detector is safe in STANDBY and DCS local
	
* **RPC: do nothing, call the DOC** --> the detector is safe 
		- If you cannot reach the DOC, do not panic, Keep going until the end of step 3, then try to call the DOC again. 
	
* **CSC-: do nothing** --> the detector is safe
	
* **CSC+: do nothing** --> the detector is safe
	
* **GEM: do nothing, call the DOC** --> the detector is NOT safe 
		- If you cannot reach the DOC, continue to call [GEM experts](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBContactListDetails1#GeM) at the link 
	
* **BRIL: do nothing, call the DOC** 

**Step 3**: As soon as Run Stopped and all DCS partitions READY FOR RAMP:

   * Shift leader call 165000 to say CMS is READY FOR RAMP.
   * Shift leader call Noemi (161681) or Zoltan to monitor the closing system for HF
   * Shift leader call any DOC you couldn't reach in Step 2.

Continue with next step... 


### During magnet ramp... 

**Step 4**: Do nothing. Do not start a run, just wait for the ramp to finish. 


### After magnet ramp finishes... 

**Step 5**: Shift leader call 165000 to confirm that the magnet ramp has actually finished and we are stable.

**Step 6**: Shift Leader do the following...

   * Pixels: do nothing
   * Strips: do nothing
   * ECAL: call the DOC
   * HCAL: do nothing
   * DT: call the DOC
   * RPC: call the DOC
   * CSC: do nothing
   * GEM: call the DOC 
    


### Point 6 Compensator/Harmonic Filter
 The BE-OP-TI (Beams Operations) group has agreed to alert us prior to switching harmonic compensator hardware at Point 6 in or out of the electrical network. This is only an alert for our information and does not require action on our part.

The shift leader will be alerted by a call of the following form: "We are preparing for a compensator switchover at Point 6 at **time** or **in x minutes**." The following acronyms may be substituted for the term "compensator"

   * Harmonic Filter
   * VAR Compensator
   * Static Compensator
   * SVC
   * TCR 

In the shift log, the shift leader will note the time the call was received and the expected time of the switchover 








