## Contact List/Phone Numbers:

| Main Numbers                          |                      |
| ------------------------------------- | -------------------- |
| Fire Brigade-Emergency, accident, pollution:    | 7 4444-0041 22 767 4444    |
| Fire Fighter at P5 (working hours)              | 16 5555                    |
| Technical Coordination on-call                  | 16 5000-0041 75 411 5000   |


| Backup                  |                      |
|-------------------------|----------------------|
| Tristan LOISEAU         | 16 2426              |
| Zoltan SZILLASI         | 16 5114              |
| Michele BIANCO          | 16 0307              |
| Wolfram ZEUNER          | 16 5338              |
| Roberto PERRUZZA        | 16 0863              |


| TI Technical Control Room   | 7 2201           |
|-----------------------------|------------------|
| Technical Shifter desk (your desk)          | 75140 - 0041 22 767 5140       |
| Shift Leader desk                           | 77705                          |
| CCC Accelerator Operations (LHC Operation)  | 77600                          |
| CERN Security agents (CSA)                  | 78877 / 78878                  |


| Rack problem                                        |                               |
|-----------------------------------------------------|-------------------------------|
| - Rack goes off or into error                       | Call subdetector rack’s owner |
| - In addition, during working hours                 | 16 5219 (Houari Sabba)        |


| Cooling problem (one of the boxes is not green)     |                               |
|-----------------------------------------------------|-------------------------------|
| - During working hours, call CMS cooling expert     | 16 3021 (Backup 16 3618)      |
| - Outside working hours:                            |                               |
| 	o Top parts                                   | 7 2201 (TI-CCC)               |
| 	o Bottom parts                                | 16 5000 (TC)                  |
| - If red screen is blinking                         | 7 2201 (TI-CCC)               |


| Magnet problem			| 			                |
|--------------------------------------|--------------------------------------|
| Magnet piquet			| 16 2082 (Backup 16 4587 Benoit Cure) |
| Magnet expert			| 16 5506                              |
| Magnet power converter 		| 77600                                |

| Cryogenic problem         |             |
|---------------------------|-------------|
| Cryogenic piquet          | 70463       |



| DCS or CMS Online problem       |                                                   |
|---------------------------------|---------------------------------------------------|
| Central DCS: DCS Expert on-call | 16 2229  (or 16 3919 during Technical Stops)      |
| Subdetectors DCS                | Call subdetectors                                 |

| DSS problem                   |                         |
|-------------------------------|-------------------------|
| Software (working hours)      | 16 3146 (Alain Meynet) or 16 5219 (Houari Sabba)   |
| Hardware                      | 16 2082 (as magnet)     |

| IT (working hours)              | 16 4317 |
|---------------------------------|---------|

| Infrastructure (working hours)  | 16 7443 |
|---------------------------------|---------|


### Subsystems Numbers

###### DCS Expert on-call
16 2229 (Backup 16 3919 Frank Glege, 16 2045 Cristina Vazquez Velez) 

###### DAQ DOC
76600

###### BRIL
16 5501 (Backup 16 7676 BRM/BCM/PLT) 

Experts:
Paul Lujan 16 5637 (Backup Arkady Lokhovitskiy 16 9136)

###### Tracker

Investigate first if the problem is with PIXEL or STRIPS, in case of doubt call STRIPS first.
!!! note
     * In DCS nomenclature, often STRIPS is meant as Tracker, whereas PIXEL is always mentioned directly
     
- PIXEL: 16 5502 (Backup 7 7703)
- STRIPS : 16 5503 (Backup 7 7704)
- Tracker DCS expert: 16 2740

###### ECAL

- ECAL DCS Expert: 72606
- ECAL TC: 165504 (Backup)

###### HCAL

- HCAL DCS Expert: 16 5439 (German) or 16 0684 (Maria)
- HCAL: 76090 (Backup 16 5505)


###### CASTOR

- 16 5887 (16 5834)

###### ZDC
- 16 6294


###### DT

- 79930 (Backup 16 5507)


###### RPC

- 16 5508 (or 16 2144 when there’s no DOC)

###### CSC

- 16 1972 (Backup 16 5509/16 1157/ 16 4652)

###### GEM

- 16 1558 (Backup 16 2109, 16 2680)

###### TOTEM

- 16 2299 / 16 0907 
- 77534 / 77750 (XCR – b.6394 SCX5)

###### CT-PPS

- 16 0836
- 16 0907 (Nicola Turini)
- 16 8051 (Jonathan Hollar)

###### L1-DOC

- 16 1958



### Usefun Links

- Email: [CMSSafety@CERN](cms-safety@cern.ch)
- Twiki: [Contact List](https://twiki.cern.ch/twiki/bin/viewauth/CMS/OnlineWBContactlist)
- CMS Safety web site: [Web Site](http://cmssafety.web.cern.ch/)





