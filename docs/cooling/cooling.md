## Cooling
This provides states of the CMS cooling stations. 

1. You have to check that all the temperatures and the flows are in **<span style="color: green;">Green</span>** color;
2. If cooling station **"USC55 EM"** and/or cooling station **"RACKS ED"** changes color **you should switch OFF the Wiener Crates** (S1, S2 and S4) via the rack services display (only after consulting with the Shift Leader and appropriate experts!).
3. If the color changes in another cooling station box : 
	* During the day (8h30-17h30) call the CMS piquet 16 3021 
	* Outside working hours:
	   * TOP PART of the screen
	      * **<span style="color: orange;">Orange color, write on DCS e-log</span>**,
	      * **<span style="color: red;">Red color - Call CCC 7 2201</span>**, 
	   * BOTTOM PART (flow & tank levels) More details here.
	      * **<span style="color: orange;">Orange color, 17h30/24h00 call 16 5000 24h00/7h00 ignore</span>**,
	      * **<span style="color: red;">Red color, call 16 5000</span>** 
	      
4. **If a value is blinking**: 

	* You should verify here if there are invalid CMS cooling values. In case the bullet corresponding to CMS Cooling is red, it is very likely to be a problem on the CCC side. You should call the CCC (72201) to inform them there is probably a problem with their publications and specify the invalid values you see (point to them to what you see from the link above).
	
	* If there are no invalid values in the link above, call the central DCS expert and inform about the problem.
	
	* The interface from above is slow, so you should wait about 10 minutes to make sure it is updating, before reporting the problem. 































