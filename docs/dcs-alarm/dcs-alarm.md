## DCS Alarm Screen
 As a general guideline when in PHYSICS data-taking, all variables controlled and monitored by central DCS should be **<span style="color: green;">GREEN</span>** (= ready for physics data-taking) and no alarm should be present on this screen.

An alarm will be raised and will appear in the central DCS Alarm Screen in case of any important problem for CMS.

Therefore it is very important to monitor the alarm screen during your shift and have prompt action in case of any alarm.

In case of any DCS alarm:

   1. Click on the description, identify the responsible expert and call him/her;
    
   2. Right-click on the alarm name and select the 'Alarm Help' to obtain a detailed description of the alarm and actions to take and to check the 'Checked by shifter at' column of the alarm screen. 
    

    








    
    
