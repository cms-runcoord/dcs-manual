## Description of DCS screens 

There are 4 types of displays

![DCS Screens](dcs-screens.png)

### Services Display (2 top left screens)  

It is composed of the following:

 * **Rack services**: Controls power status and monitors the temperature of the racks. It is organized by areas of the experimental and service caverns, showing the power status and temperature of each rack in these areas. 
 
 * **Cooling services**: Monitors the temperature and flow of the different CMS cooling stations. 
 
![Rack and Cooling Systems](rack&cooling.png)
 
### Central DCS
 * **Central-DCS panel including the handshake**
 
![Central panel](central-dcs.png)
 
### Alarm Screen 
(bottom left screen) 

![Alarm Screen](alarm-screen.png)


### Subdetectors
   * GEM
   * DT
   * CSC
   * RPC
   * HCAL
   * ECAL
   * Tracker (Pixel, Strips)
    

## Central-DCS instructions for Shift Leaders 

One of the central-DCS shifter tasks is to perform the DCS-Safety Tour during the night and weekends. When the central-DCS shifter leaves the shifter desk for the DCS Tour the Shift Leader is required to login to the central DCS screens using the **'Shift_Leader'** role to control and monitor the central DCS.

Shift Leaders are required to attend the central DCS tutorial sessions.

Central DCS tasks for Shift Leaders are the same as for the central DCS shifters and are outlined here. 


### Shift Procedures

#### Central Shifter Instructions

  * Read the elogs ( [Slimos Tour](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/Elog?__adfpwp_backurl=https%3A%2F%2Fcmsonline.cern.ch%3A443%2Fwebcenter%2Fportal%2Fcmsonline%2FCommon%2FElog%3F_afrRedirect%3D19326764164225507%26__adfpwp_mode.683379043%3D1&__adfpwp_action_portlet=683379043&_piref683379043.strutsAction=%2FviewSubcatMessages.do%3FcatId%3D492%26subId%3D105%26page%3D1) and [Central DCS](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog?__adfpwp_action_portlet=683379043&__adfpwp_backurl=https%3A%2F%2Fcmsonline.cern.ch%3A443%2Fwebcenter%2Fportal%2Fcmsonline%2Fpages_common%2Felog%3F__adfpwp_mode.683379043%3D1&_piref683379043.strutsAction=%2FviewSubcatMessages.do%3FcatId%3D551%26subId%3D21%26page%3D1%26fetch%3D1)) of the previous shift. 
  * Talk to the previous shifter about any specific problems that need attention. 
  * Login
  * Handle and Monitor the central DCS
  * Monitor the subdetector statuses
  * LHC_Handshake
  * Monitor the alarm screen, services (rack, cooling) and DSS screens; 
  * Perform the Safety tour; 
  * Handle Access Control; 
  * Write an entry in the DCS elog; 
  * Fill in the checklists ( edms.cern.ch/ui/#!master/navigator/document?D:1981361806:1981361806:subdocs ) 
  
#### Login
  * DCS -computer login: should be done automatically when the machine is started.
  * DCS-user login: is done via your CERN card. Place it on ( do not insert in the slit) the card reader by the DCS console. Your username and the central-DCS role should appear in all the login boxes of all the displays.
  * Verify you are logged in (your username should appear in all the displays on the left top corner) with the proper role; 
  * If the right role does not appear on a login box, select CMS_OPERATOR by clicking on the username; 
  * When you remove your cern card, it automatically releases the control of central DCS. 


###  Main central DCS panel 

#### CMS DCS CENTRAL mode 
The aim of the Main central DCS is to be able centrally to take action on subdetectors like switching ON (from STANDBY) or STANDBY (from ON) and monitoring their states. 

#### General
 The mechanism adopted to model the structure of the sub-detectors, sub-systems and hardware components in a consistent fashion is to use a hierarchical (tree-like) structure. This tree-like structure is used to send commands and report hardware alarms and status. In its hierarchy, a "parent" controls and monitors all its "children".

When taking control of a partition, you are taking control of all the nodes below it, which means that all the commands will propagate down to all its included children. When you take control of CMS DCS, any commands you give will propagate to all its included children, i.e., all the included subdetectors.

Central CMS DCS is directly connected to the subdetectors' TTC partitions. The states reported by the subdetectors' partitions are "translated" into physics related states that are reported by the central CMS DCS. 

A simplified central DCS tree can be visualized in the central DCS main panel where partitions can be put in CENTRAL or LOCAL: 

#### Start CMS DCS CENTRAL Mode
<span style="color: red;"> Always make sure that when the BEAM mode reaches INJECT PHYSICS BEAM with the aim of reaching STABLE BEAMS, that all subdetectors are in CENTRAL. </span>


In order to **start** the **<span style="color: green;"> CENTRAL</span>** mode of CMS DCS do the following steps: 
  
  1. Check with the subdetectors if they want to be controlled by central DCS and their status;
  
  2. Ask if all **included** parts can be switched **<span style="color: green;"> ON </span>**;
  
  3. The subdetector should release the control;
  
  4. Click on the corresponding sub-detector to be included in **<span style="color: green;"> CENTRAL</span>** mode (as shown below). 
  
  5. If only a part of a sub-detector should be included, click on the corresponding TTC partition; 
  
  6. Once the subdetector is in central, your username should appear as the owner of the partition on the box on the right of the panel. If it is not the case, contact the central DCS expert on-call;
  
  7. CMS DCS State should then be in **<span style="color: blue;"> NOT_READY</span>**. 
  
 When CMS DCS is in CENTRAL mode, you can take action :

**To switch ON** : Select the ***GO_TO_PHYSICS*** option from the drop-down menu of each subdetector, on the left of the central DCS panel. By doing this, all the partitions of a given subdetector will go to ***GOING_TO_PHYSICS*** shortly followed by **<span style="color: green;"> READY_FOR_PHYSICS</span>**.

**To Switch ON only a part of a subdetector**: Select the desired state directly from the TTC partition, as shown on the picture below:

Central DCS local to central 

**To Put In Standby** : Select the ***GO_TO_STANDBY*** option from the subdetectors drop-down menu on the left of the central DCS panel. 

#### Exit CMS DCS CENTRAL Mode
**<span style="color: red;"> Always make sure that when the BEAM mode reaches INJECT PHYSICS BEAM with the aim of reaching STABLE BEAMS, that all subdetectors are in CENTRAL.</span>**

In order to ***EXIT*** CMS DCS CENTRAL mode: select the LOCAL mode of the corresponding subdector. 

#### FSM Color Codes and Padlock Information 

In this section you will find a description of the FSM states and color codes as well as possible padlock configurations. 

 * FSM States generic color codes:

    - **<span style="color: green;"> ON: green</span>**;
    - **<span style="color: blue;"> OFF/STAND_BY: blue</span>**;
    - **<span style="color: red;"> ERROR: red</span>**. 

* The available **states of Central DCS** are:

    - **<span style="color: green;"> PHYSICS </span>** - ready for Physics;
    - **<span style="color: blue;"> OFF </span>** - all voltages are switched OFF;
    - **<span style="color: blue;"> STANDBY </span>** - subdetectors in STANDBY, usually LV ON, HV OFF but it varies from subdetector to subdetector;
    - **<span style="color: blue;"> NOT_READY </span>** - initial state;
    - **<span style="color: red;"> ERROR </span>** - there is a system is in ERROR state, not ready for data-taking. Manual intervention has to be done. Call the corresponding expert;
    - **GOING_TO_PHYSICS** - State Transition;
    - **DEAD** - the FSM is not running, call a PVSS expert! 

* The available **actions of Central DCS** are:

    - **<span style="color: green;">GO_TO_PHYSICS</span>** - switch ON all included sub-detectors;
    - **<span style="color: blue;">STANDBY</span>** - put all included sub-detectors in STANDBY;
    - **<span style="color: blue;">SUPEND</span>** - set all included sub-detectors to NOT_READY state; 

* The available states of the subdetectors' TTC partitions are:

    - **<span style="color: green;">ON </span>**- The partition is ready for data-taking;
    - **<span style="color: blue;">STANDBY</span>** - Ready for beam injection (usually means HV OFF and LV ON);
    - **<span style="color: blue;">OFF </span>**- all voltages are switched OFF;
    - **<span style="color: red;">ERROR </span>**- system is in ERROR state, not ready for data-taking. Manual intervention has to be done. Call the corresponding expert. 



The padlockbutton indicatesthe control status. It canappear in different ways:

- **<span style="color: gray;">Open grey padlock </span>**: no owner on the offending system. **<span style="color: red;">The FSM states will not be reported consistently until you take control</span>**; 

- **<span style="color: green;">Closed green padlock</span>**: panel owns; 

- **<span style="color: red;">Closed red padlock</span>**: somebody else owns; 

- Padlock with a diagonal line: Locked out. It means that nobody else can take control (or send commands) to the partition but the owner. In the picture, the red padlock means somebody else has the control of it and at the same time it is locked Out.

- Yellow border: some lower level not included. 

- **<span style="color: blue;">Blue padlock</span>**: control shared. 

When the mouse is passed over a padlock, a “bubble” yellow frame with the control and mode status appear: 

- The mode can be the following: 
   * Included: Child fully controlled by parent. Commands propagate down. States propagate up. 
   * Excluded: Child not controlled by parent. Commands do not propagate down. States do not propagate up. 
   * Manual: parent does not send commands. Commands do not propagate down. States do propagate up. 
   * Ignored: Parent ignores the child's states. Commands propagate down. States do not propagate up.


- Owner: Who has the control
- User: 
- Comment: In the above picture, the fact that the partition has Disabled Children appears. 

You can change the status by clicking on the padlock. 









