## DSS Tasks
Check the DSS screens (as shown below) for alarms:

In case of alarms:

   * **In case there is any siren (light + sound) alarm, quickly check the DSS alarm panel, the alarm screen, silence the alarm, call 16 5000 and then inform the Shift Leader.**
  
   * Read the alarm description and contact the responsible person (e.g. Magnet, subdetectors, etc.);
    
   * In case the alarm description is not clear:
   	- During working hours: call DSS expert, Alain Meynet: **16 3146**;
   	- Out of working hours: call, Wolfram Zeuner: **16 2541** and at last **16 5000** (in case the other numbers failed). 

Check the DSS display is Alive: The buttons should blink. If not, restart the panel (instructions in the DSS binder). If restart does not work, call: 162082 


**What to check on the DSS alarm screen:**
![DSS Alarm Screen](DSSalarmScreen.png)


Slide of DSS alarm screen


























