## ELOG Procedures

 You must report any problems in the elog that occur during your shift in [this link](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog)
 
 First access to the CMS Online Site > Common > Elog
 
 ![CMS Online Site](1.png)
 
 Subsystems > DCS
 ![2](2.png)
 
 DCS!!!
 ![3](3.png)


   * **At the beginning of your shift**: Write an elog with the status of all systems with the Type: Begining of Shift.
   
![4](4.png)
   
   * **During your shift**: Write a short elog with specific information when you have any problem with sub-detectors, cooling, racks, etc... with an indication in your elog in the Title of the problem.
   
   * **At the end of your shift**: Write a summary elog with all that happened during your shift: Shift Summary
