## Ghosts
Due to various reasons, the access system can show “ghosts”: persons underground who have already left the caverns (improper usage of the PAD; entering with a dosimeter and exiting via the MAD, problems with PADs, etc).

At the beginning of your shift (especially nightshift), you should therefore check whether people are still logged into the access system. 

!!! note
     * Ghosts are highlighted with **<span style="color: orange;">Orange</span>** rows.


If you find ghosts at the beginning of your shift, you should:

1. Check whether these ghosts have been dealt with by the previous shifter. If so, skip the next points of this instruction.

2. Try to reach the ghosts via a mobile phone, an office phone and email.

3. Report to TC on-call at 165000.

4. Only with the permission of TC on-call165000: make an underground tour to make sure the caverns are empty.

5. Make a corresponding Elog entry.
