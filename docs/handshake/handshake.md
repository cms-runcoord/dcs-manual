## LHC Handshake (LHS)
When LHC is about to do an action in the machine which requires a handshake with the experiments (inject beam, beam dump or beam adjust), it sends a WARNING to CMS via the LHC Handshake managed from the central DCS panel (the picture below shows the LHC handshake part of the central DCS panel). 

![Handshake](handshake.png)

This panel contains information about the **handshake mode**, the **LHC handshake conditions** (represented by a diagram), the communication via the Data Interchange Protocol (DIP) between CMS and the LHC, the **type of handshake** and the **problem report**. 

### Handshake Mode 
Can be set to **MANUAL** or **AUTOMATIC**: 

  * In the **MANUAL mode**, the CMS readiness is sent manually by the shifter by clicking on the 'CONFIRM CMS IS READY' button. 
  ![LHS Manual MODE](LHSmanualMode.png)

  * In the **AUTOMATIC mode**, the CMS readiness is sent automatically. 
  
  
### LHC handshake diagram 
During the handshake, a diagram summarizes the current condition of the handshake. 

![Handshake](handshake2.png)

  * By clicking on the boxes "preparing protection" or "protection ready", you can see the details of the replies from all systems to the **protection mechanism** (automatic check of the subdetectors' Safe Condition for Beam Injection). CMS READY confirmation can only be sent after the protection mechanism has gotten the OK from all the subdetectors. 
  	- If the LHC handshake is in MANUAL mode, the 'CONFIRM CMS IS READY' button can only be pressed after confirmation that the subdetectors are in a safe state (according to the [Automation Matrix](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/DCS/Automation), indicated if the 'protection ready' box of the diagram is highlighted) and consultation and agreement with the Shift Leader. 
  	![LHS CMS Ready](LHSCMSready.png)
  	
  	- If in AUTOMATIC mode, the CMS readiness will be sent automatically once the protection mechanism has gotten all the replies from the subsystems. Nevertheless, the shifter should watch the whole procedure and make sure that when the READY command is sent, all subdetectors are in a safe state. 
  	
  * The **'&'** means that all the conditions must be true to go to the next state (All experiments confirmed to be ready). This means CMS has to answer ready by software, the **INJECTION INHIBIT button** has to be released and LHC has to pass through **IMMINENT**. 
When the shiftleader has pulled the INJECTION INHIBIT button the CMS Injection button status is yellow and indicates **INJECT ALLOWED**. 

### DIP communication between LHC and CMS 
It is shown in the LHS box. Should be always green, if not call the central DCS expert. 
![LHS Box (DIP)](LHSbox.png)

### Handshake type
The type of handshake ongoing is highlighted in the LHS box. It can be injection, adjust or dump. If there is no ongoing handshake, the three options should be on a white background. 
![LHS Type](LHStype.png)

### LHS problem report
During a handshake there is a possibility of reporting a problem to LHC, by clicking the 'REPORT PROBLEM TO LHC' button. 
![LHS Report a Problem](LHSreportProblem.png)

### LHC Handshake for Beam Injection
 We describe here the sequence for the beam injection. A similar procedure has to be followed for the other operations except for the inhibit button on DSS hardware panel.

When LHC is about to perform beam injection in the machine, the 'phase warning' box of the LHS diagram is highlighted and the buttons 'CONFIRM CMS IS READY' (if the Handshake Mode is set to MANUAL) and 'REPORT PROBLEM TO LHC' are made available in the panel.
![LHS Warning](LHSwarning.png)

CMS has to respond to the LHC handshake warning by sending its readiness state to LHC (via the LHC Handshake).

In order to do the LHC Handshake, one has to:

  1. Confirm that each subdetector is READY for beam injection (by checking that all responses to the protection mechanism are in the OK state and verifying the [Automation Matrix](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/DCS/Automation))
  ![Protection Mechanism](protectionMechanism.png) 

  2. Consult with the Shift Leader and get his/her agreement to confirm CMS readiness

  3. Click on the button 'CONFIRM CMS IS READY' (on MANUAL mode only)

  4. Ask shift Leader to pull the HARDWARE injection inhibit button, upon confirmation with all subdetectors on their READY for beam injection state

  5. Once the LHS cycle is done, the only highlighted box in the LHS diagram should be the 'standby' one.
  ![LHS Standy](LHSstandby.png) 


!!! note
     * **STANDBY** : no handshake ongoing
    
     * **WARNING** : LHC gave a warning. This is automatically confirmed so the state moves to WARN_CONFIRMED 
   
     * **IMMINENT** : if LHC gave a IMMINENT and still CMS did not reply ready
   
     * **WARNING_READY** and **IMMINENT_READY**: this is when LHC gave a WARNING or an IMMINENT and CMS confirmed to be READY
   



### Checklist for turning the Tracker ON
 When the beam mode reaches STABLE BEAMS with machine mode of PROTON PHYSICS, the Shift Leader and DCS shifter should check the state of the Pixel and Strips detectors.

**<span style="color: green;">Important</span>**: the communication between the DCS shifter and Shift Leader is crucial for good operations! 

1. **Shift leader and DCS shifter confirm that the Pixels and Tracker have automatically turned ON at Stable Beams**. 
   * If after ~1.5 minutes of STABLE BEAMS they have NOT turned to **100% ON** and/or are stuck in GOING_TO_PHYSICS state:
   
      - send the GO_TO_PHYSICS command once (to the corresponding partition) 
      
      - if sending the GO_TO_PHYSICS command once does not bring the specific sub-detector ON, the Shift Leader should call the corresponding Pixel and or Strips DOC. 
      
   * In case they stay in STANDBY, follow the checklist below and/or check the [Shift Leader checklist](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBShiftLeaderInstructionsPhysicsRunningRamp) under the 'Beam Mode = Stable Beams' section. 
   
**Always cross-check this checklist with the Shift Leader checklist under the 'Beam Mode = Stable Beams' section**.


### Checklist for turning the Tracker ON Manually: 
1. The shift leader and the DCS shifter check that the beam conditions are acceptable to turn ON the Pixel and Strip HV. For detailed instructions for the Pixel please read [this document](https://twiki.cern.ch/twiki/pub/CMS/PixelOperations/PIXEL-HV-Procedures.pdf). The Shift Leader should keep an eye on the backgrounds before stable beams are declared in order to quickly be able to decide if beam conditions are good. Here is the check list:
	* INJECTION must be inhibited
	* BKGD1 must be below 20. [see here](https://op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC3).
	* BKGD2 must be below 20. [see here](https://op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC3).
	* BKGD3 must be below 50. [see here](https://op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC3).
	* BEAM MODE must be STABLE BEAMS 
	* If any of these conditions are not met call **<span style="color: red;">BRM 165501, Pixel 165502 and Strips 165503</span>**
2. When the conditions for turning on the HV is met, the DCS shifter turns on the HV
3. Ramp up <1 min for Pixels approximately 10 more seconds for Strips

**BKGD1** and **BKGD3** can be verified in the links given above. Find below, an example picture of the values to be verified: 

![BKG1&BKG2](lhc-operation.png)

### FSC HV Operations
**<span style="color: red;">When LHC goes into STABLE BEAMS, the FSC HV will be turned ON automatically. It follows the same automation matrix as for the ZDC.</span>**

If there are any problems with the FSC, please call Sorina Popescu at 163242. 


### Monitor the CMS Subdetectors: (check their status) 
 Make sure CMS DCS is always in **<span style="color: green;">READY_FOR_PHYSICS</span>**. If an error appears in any subdetector, CMS DCS will go into **<span style="color: red;">ERROR</span>**. You have to investigate to which partition it is related to and contact the corresponding subdetector shifter. (You can navigate through the concerned subdetector DCS display (on the right screens) to investigate the problem but you are not supposed to take any action on it.)

In the next subsections there are instructions on what to monitor for each subdetector, when in CENTRAL mode. 

#### Pixel detector monitoring

 You always have to look at the CMS tracker page. As a general guideline you should call the Pixel DOC (16 5502) if you notice anything anomalous.

**For interfill periods (NO BEAM in the machine) The Pixel High Voltage is ramped up automatically with the BEAM_MODE = RAMP_DOWN (and accelerator mode = PROTON_PHYSICS). The High Voltage will be ramped back down automatically once the injection handshake comes. The safe states and automatic actions are outlined in the [Automation Matrix](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/DCS/Automation).**

 <span style="color: red;">Transitions from OFF to STDBY or Standby to OFF should only be done by Tracker people.</span>

<span style="color: red;">If the pixel detector goes **OFF** for any given reason **call the pixel DOC 16 5502**. The Pixel detector should be automatically locked to the OFF state and a Pixel expert intervention is needed to turn the detector back to STDBY or ON. **If the pixel is not locked to the OFF state (DCS software fault) still a pixel expert intervention is needed so do not turn it to STDBY**.</span>

There are two different conditions in which the pixel detector can be released to Central DCS and each of these condition have different instructions: 

* **Dormant State**: Pixel detector is OFF and you should just watch the state (if not OFF call 16 5502) and check the dew points (see below): 
* **Operational State**: Pixel detector is either ON or STDBY and you should check the status of the power system and of the environment with the help of the attached picture and the directions and limits listed below. 
* **Table 1**: from left to right 
   - **State**: the state of the pixel should be ON or STDBY (for any other state call 16 5502) 
   -  **rdHV**: this number should always be either 100 and GREEN or 0 and red (for any other combination call 16 5502)
   - **rdLV**: this number should always be either 100 and GREEN or 0 and red (for any other combination call 16 5502) (see **LV Note** below)
   - **HVon**: this number should be either 100 and GREEN (when ON) or 0 and BLUE (when STDBY) (For any other combination call 16 5502)
   - **HVerr**: this number should be 0.00 and WHITE (For any other combination call 16 5502)
   - **LVon**: this number should always be 100 and GREEN (for any other combination call 16 5502)(see **LV Note** below)
   - **LVerr**: this number should be 0.00 and WHITE (For any other combination call 16 5502)(see **LV Note** below)
   - **Ctrlon**: this number should always be 100 and GREEN (for any other combination call 16 5502)
   - **Ctrlerr**: this number should be 0.00 and WHITE (For any other combination call 16 5502)
   - **Average T**: Ignore
   - **MaxT**: This number should be below 30 deg C and WHITE (For any other combination call 16 5502)
   - **LV Note**: Do not issue any Go To Physics or Standby until the Pixel DOC is called 16 5502. Do not turn on LV channels. 
* **HB** (heart beat): The Pixel heart should be RED, pulsing and not broken. (For any other combination call Tracker DCS on call at 16 2740) 
* **DEW POINTS**: The two (2) numbers displayed should always be on a WHITE background and Below -20 deg C. (For any other combination call 16 5502) 
* **Central DCS Alarm Panel**: There should be no Pixel alarm in the panel. If any alarm appears with text in the **Device DP Element** or **Description** column that includes "Pixel" (PixelEndCap or PixelBarrel), call 16 5502 


#### Strip Tracker detector monitoring
As a general guideline, if you notice anything anomalous with the Strip Tracker contact the Strip Tracker DOC: 16 5503 (if unreachable call 77704).

Normally the Strip Tracker detector will be either ON, STDBY or OFF-LOCK.

<span style="color: red;"> If the **Strips detector goes OFF** for any given reason **call the Strips DOC 16 5503**. The detector will go to a 'locked' state that has to be removed by the DOC. Whenever the LHC mode allows the detector to be back in standby shift leader should call Strips DOC to get authorization to be back in standby and to prepare for data taking.</span>

**<span style="color: red;">The Shift Leader should call the strips DOC (16 5503) whenever the beam mode goes to ADJUST (if the aim of the fill is to deliver STABLE BEAM). Both you and the central shift leader are in charge of raising the HV ON for the strips: see the 'Checklist for turning ON the Tracker' for instructions on what to verify before switching the detector ON. If the detector is for some reason completely OFF (not in STANDBY) at that stage, then the strip tracker DOC must be called and his agreement given to turn back ON.</span>**

You always have to react to Strip Tracker alarms (ALARM SCREEN) by contacting the strip DOC 16 5503. The Strip DOC will take care of the alarm and clear it as soon as he documented it.

Further you should check at least every second hour the CMS tracker page1 for the power system status and for the environment with the help of the attached picture and the direction and limits listed below.

* **FSM**: from left to right 
   - **State**: the state of the TIB | TOB | TECs should be ON or STDBY (for any other state call 16 5503) 
   -  **rdHV**: this number should always be either 100 and GREEN or 0 and red (for any other combination call 16 5503)
   - **rdLV**: this number should always be either 100 and GREEN or 0 and red (for any other combination call 16 5503)
   - **HVerr**: this number should be 0.00 and WHITE (For any other combination call 16 5503)
   - **LVerr**: this number should be 0.00 and WHITE (For any other combination call 16 5503)
   - **Ctrlerr**: this number should be 0.00 and WHITE (For any other combination call 16 5503)
   - **MaxT**, **MinT**: This numbers should always be on a WHITE background (For any other combination call 16 5503) 
* **Heart beat**: ALL hearts should be RED, pulsing and not broken (For any other combination call 16 5503) 
* **DEW POINTS**: All numbers displayed should always be on a WHITE background. (For any other combination call 16 5503) 


#### ECAL = EB/EE/ES detector monitoring 
Instructions (updated for 2016!) on what to monitor regarding the EB/EE/ES sub-detectors can be found [here](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/ECALpage1.png) and [here](https://twiki.cern.ch/twiki/pub/CMS/EcalFTC/CMS_ECAL_cDCS_Instructions.pdf). 

 Under normal circumstances, during LHC Physics operating periods,
**ECAL = EB/EE/ES (Barrel, Endcap and Preshower; Plus and Minus halves) should be in DCS State “ON” at all times.**

In case of problems with the EB or EE or ES **DCS State**, please call the **ECAL DCS on-call** (**72606**, else 70827).

In case of problems with the EB or EE or ES **Racks**, please call the **ECAL Field-Technical-Coordinator** (**16 5504**).

In both cases please also inform the Shift Leader.

Note that the CMS DCS Shifter should have the capability to turn any part of ECAL OFF, but is UNABLE to turn any part of ECAL back ON.

NEW **Last updated 22-July-16: ECAL Field Technical Coordination 16-5504**

 If you receive on the Alert Screen an ECAL alarm “*Cooling problem!*” from Datapoint “*ECAL/somewhere/ESS*” please be very reactive: **Call ECAL-DCS (70827 / 72606), else ECAL-TC (16-5504) IMMEDIATELY** and ask them to check that the interlock has been effective. Inform the shift-leader and 16-5000 immediately. If ECAL experts are not reachable, stay in contact with 16-5000 retrying to reach ECAL.

If no confirmation within 15 mins of the cooling alarm that the situation is under control, **inform the shift-leader**, and from Central_DCS Main Panel: *do for EE−, EB−, EB+ and EE+: “GO_TO_OFF”**. This should turn OFF all EB & EE. It should crash any ongoing DAQ Run that uses them. Then wait for ~2 minutes.

**If** no confirmation within 20 mins of the cooling alarm that the situation is under control, **or if *you receive a clear instruction to do it (*and only in these cases)**, **<span style="color: red;">shut down “EB+EE” from the DSS hardware panel</span>** between the windows (Sub Detector Safe panel at the bottom left).

You need first to take the key “SYNOPTIQUE DSS Enable action “ in the blue box, switch it on in the lock on the side of the panel and finally turn on the EB/EE button in the “Sub Detector Safe” panel (see pictures below). 

This will turn OFF the power to
a) the Racks containing the HV crates, and
b) the power filters (OPFCs) feeding the Maraton power supplies.
Some 11 Racks will go OFF in your Rack Status Display. 


#### HCAL detector monitoring
In case of problems with the [HCAL](https://twiki.cern.ch/twiki/bin/view/CMS/HCAL/WebHome) detector, call the HCAL DOC (**76090**) or the HCAL DCS Expert (**16 5439, 168898 or 160684**). 

NOTE: It is normal that the two racks S2C12 and S2C13, that belong to HCAL, stay OFF, as they don't contain any equipment that needs to be ON at the moment. 


#### DT detector monitoring
 Instructions on what to monitor regarding the DT sub-detectors can be found [here](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/DT_central_DCS_Instructions_V6.pdf).

'Majority' table added in DT_Detector panel. DON'T CALL THE DT DOC if the percentage of HV channels ON becomes < 100% for few seconds (sporadic discharges in some channels can have this effect).

In case of problems with the DT detector, call the DT DOC (**79930**) as indicated in the document above. 

#### RPC detector monitoring

Instructions on what to monitor regarding the RPC sub-detector can be found here. In case of problems with the RPC detector, call the RPC DOC (**16 5508**) as indicated in the document above. 


#### CSC detector monitoring

Instructions on what to monitor regarding the CSC sub-detector can be found here. In case of problems with the CSC detector, call the CSC DOC (**16 1972** or 16 5509) as indicated in the document above. For quick reference of possible problems and required actions you can consult the action matrix here




















