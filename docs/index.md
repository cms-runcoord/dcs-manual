# Central DCS Instructions

The general task of the Central- Technical DCS shifter is to make sure that CMS is ready for Physics data-taking by monitoring the Central DCS, the services, communicating with the subdetectors' shifters/experts, and turning the subdetectors ON from STANDBY, or STANDBY from ON. 
	
   - <span style="color: blue;">Central Technical Shifter (ex DCS) desk @ P5 phone number: 7 5140.</span>
   
   - <span style="color: blue;">Technical Shifter (ex DCS) Expert on Call: 16 2229. </span>

## Becoming a Central-Technical Shifter (DCS) Tutorials 
**<span style="color: red;"> !!!!! MANDATORY: Read the Technical shifter training requirements [HERE](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/CMS_Technical-Shifter_requirements.pdf) !!!!!
(note that during LS2 we require 1 trainee shift instead of two)</span>**

We expect you to plan your trip to CERN so that you can follow:

1. The Technical Shifter Tutorial (one every 4 weeks)

2. The Self-Rescue Mask training

3. Get a dosimeter (or a token) at the dosimetry service

4. Attend 2 trainee (shadow) shifts

** We can help you organize yourself, but no one will be excused for not knowing these requirements. **


##ATTENTION
• **<span style="color: red;">DO NOT</span>** schedule your first shift or two during the 11pm-7am period (night shift). This should only happen after you have gained sufficient experience with the central shifts.

• During the **Shutdown Period**, please read this version of the Technical Shifter tutorial [pptx](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/Technical_shifter_tutorial_2019.pptx) or [pdf](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/Technical_shifter_tutorial_2019.pdf).

• During the **Run Period**, please read this version of the Technical Shifter tutorial [pptx](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/CMS_central_Technical_Shifter_tutorial_V2.15_2017_compressed.pptx) or [pdf](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/CMS_central_Technical_Shifter_tutorial_V2.15_2017_compressed.pdf).

• During the **Run period**, you can find the appropriate Safety Tour itinerary [pptx](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/safetytour_RUN2_v2_5_0.pptx) or [pdf](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/safetytour_RUN2_v2_5_0.pdf).

The normal sound of the pump from the magnet area can be found in [wav](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/Pump_MagnetArea_NormalSound.wav)



