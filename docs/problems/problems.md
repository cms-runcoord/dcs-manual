## Actions to take in case of problems

The most common problems are explained below as well as the definition of the various actions to be taken in case of problems. When in doubt, contact the expert on-call.

**In case there is any siren (light + sound) alarm, quickly check the DSS alarm panel, the alarm screen, silence the alarm, call 16 5000 and then inform the Shift Leader.**

### Rack Problems
 In normal operations all racks should be ON and GREEN.

   * **Power off Problem**: Call the owner of the rack (subdectector shifter), try to find out why the rack is switched off and ask the shift leader the permission to switch back on. If a rack is ON and goes to **OFF**, contact the rack's owner **and** verify with DAQ shifter and the Shift Leader if they started seeing ERRORS from the offending subdetector.
 
   * **DSS Problem concerning Racks**: check the DSS screens and follow the DSS alarm handling;

   * **Temperature Problem**: Check if there is a problem on cooling station, if not go to see the rack and look if there is something abnormal - if Yes : Call the shift leader - if not : call the owner of the rack (subdectector shifter) and inform the shift leader. 

   * **More than 5 racks are switched off**: Before start switch on procedure, inform the Shift leader and 165000.
    Before turn on racks, you have to acknowledge all DCS then all DSS alarm.
    
    You can follow the manual to switch On racks after massive DSS alarms. If racks in S4 and UXC are off, switch on the racks on S4 first (these are the LV for racks in UXC)


#### PROTOCOL TO SWITCH ON RACKS AFTER A MASSIVE DSS ALARM THROUGH THE CURRENT DCS RACK APP WIZARD (July 2018)

Through the DCS Rack Wizard, the shifter is able to do all actions related to power ON racks. Switching OFF is restricted for safety reasons to Rack Control Expert egroup members.
Follow this in strict order:

1. **SEND ACKNOWLEDGEMENT TO ALL RACKS**: On first combo box select “Send ack to some racks(s)”.
Then on second combo box select “All racks that are not in ON state”. Or you can break it in several actions, sending the commands to all the racks not in ON that are in a certain zone (S1,S2,X1, USC,UXC) or that have been assigned to a certain subdetector (the third combo box will let you choose which zone or subdetector).
You can also select here “You want to select racks manually” and later on select from the populated left table the racks that should eventually be commanded.
After this, the left table should be populated with all the racks that meet the selected criteria.
Now you must select out of the left table which of them are being sent the command. Only the racks on the right table will be commanded.
Select the racks you want from the left table and click the > and < button to move racks within tables. You can pass all the racks in one table to the other by using the >> and << buttons. 
<span style="color: red;">Commands will not be executed till you don’t press the EXECUTE button depicted below. Again, commands will only be sent to the racks on the right table. Once you press EXECUTE there is no way to cancel the commands sent. But you can press the CANCEL button at any moment before pressing EXECUTE to reset the process and start over again.</span> 

2. **WAIT FOR ALL THE ACKA COMMANDS TO BE SENT TO THE PLC.**
Once you click the EXECUTE button, the commands to be sent will be added on the queues for the PLCs (see below)…and progressively they will be sent to the corresponding PLC (and be deleted from its queue). 

3. **ACKNOWLEDGE BOTH Alarms and outputs related with all the racks affected on the DSS. DON’T FORGET TO ACKNOWLEDGE THE OUTPUTS! THIS SHOULD BE DONE AT DSS LEVEL, NOT IN DCS.**
It’s preferable to wait till point 2 has been completed to perform this action as if DSS acknowledgement for a rack happens to arrive before the ACKA command has been applied in its PLC, its DSS acknowledgement should be repeated at this point.

4. **SWITCH ON THE RACKS THROUGH THE DCS:**
Follow same instructions depicted in point 1 but selecting on first combo box the option “Switch on some racks(w) (turbines and LVs)” instead.
Commands will again be queued and will be sent to the corresponding PLC eventually.

If this does not happen to switch on a rack, the state of the different signals in the DCS and in DSS for that rack should be analysed using that Rack Operation Panel to understand why… but normally, by simply repeating the same operation again using the Wizard it should work.

Generally the malfunction is due to bad coordination between DSS and DCS ACKNOWLEDGEMENT (DSS outputs not acknowledged or DSS acknowledgement to happen before DCS acknowledgement).

So BE SURE THAT BEFORE ACKNOWLEDGING DSS ALARMS/OUTPUTS, POINT 2 HAS ALREADY BEEN COMPLETED (this means that ALL ACKA commands have left for the PLCs… in massive ACKA this could take some time if it implies lots of racks).

Acknowledgement of the DSS outputs won’t really reset the DSS interlock if previously the corresponding rack default has not been acknowledged at PLC level. If this is the case, DSS outputs will still be set, and would need to be acknowledged on DSS again after the ACKA at PLC level is completed. 

* **Explanation for rack switch on procedure**: in general terms, performing this: ACKA at DCS level, then at DSS level (alarms + ALSO OUTPUTS), and then an ON command (if UXC) or a ACKA(redundant most of the times)+REARM command (if USC) should switch on a rack ALWAYS if there are no further problems.
If this does not work, the rack normally has some hardware problems 



### Cooling Station Problems 

In normal operations, all cooling values should be GREEN.

If the color changes in another cooling station box :
* During the day (8h30-17h30) call the CMS piquet 16 3021

* Outside working hour, if cooling box red call CCC 7 2201, except for bottom line (flow & tank levels) Orange color, 17h30/24h00 call 16 5000 24h00/7h00 ignore, *Red color, call 16 5000 

Service screen values are blinking. Actions:

* You should verify here if there are invalid CMS cooling values. In case the bullet corresponding to CMS Cooling is red, it is very likely to be a problem on the CCC side. You should call the CCC (72201) to inform them there is probably a problem with their publications and specify the invalid values you see (point to them to what you see from the link above)

* If there are no invalid values in the link above, call the central DCS expert and inform about the problem.

* The interface from above is slow, so you should wait about 10 minutes to make sure it is updating, before reporting the problem. 



First Number to call in case of problem (to be called at all times) **CCC: 7 2201**;

Additional contact number (to be called **after** CCC has been called): Gilles DUCRET (EN CV): 16 5944. 

### Magnet Problems 

* Magnet problems are reported by DSS.
* First Number to call in case of problem (to be called at all times) **CCC: 7 2201**.
* Additional numbers:
	- Cryogenic problem : 16 0124
	- Magnet problem : 16 2082 

### Subdetector Problems
For any problem noticed in a Subdetector DCS panel, contact the Subdetector DOC:
* Strip Tracker: 165503 (backup - 77704)

* Pixel: 165502 (backup - 77703)

* ECAL: 72606 (backup - 70130). **For ECAL only**: if detector Safety- or Technical-Coordination- problem call 165504.

* HCAL: 76090 (backup - 165505).

* DT: 79930,165507

* RPC: 165508

* CSC: 165509

* BRM/BCM: 165501


### DCS Problems 

For problems with the DCS system itself (screen or mouse frozen, window disappeared, loss of communication, etc.) call the Central DCS expert on-call number or, if unreacheable, one of the Central DCS team experts numbers:

   * DCS expert on Call: 16 2229
   * Frank Glege (Central DCS team): 7 1688, 16 3919
   * Lorenzo Masetti (Central DCS team): 16 5820
   * Raul Estupinan (Central DCS team): 16 0633 


### DSS Problems/Alarms

* **DSS Alarms**:
	
   * In case of any SIREN alarm, quickly check the DSS alarm panel, alarm screen, silence the alarm and then call **16 5000**.

   * In case of alarms listed on the DSS screen:

    * Read the alarm description and contact the responsible person (e.g. Magnet, subdetectors, etc.);

    * In case the alarm description is not clear:

       - during working hours: call DSS expert, Alain Meynet: **16 3146**;

       - out of working hours: call, Wolfram Zeuner: **16 2541** and at last **16 5000** (in case the other numbers failed). 

* DSS problems:
   * In case of Hardware problem, call **16 2082**; 








