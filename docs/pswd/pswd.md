## Passwords

A number of passwords can be needed during your shift (for instance, if a computer has been restarted). There is a blue box behind the shiftleader's monitors which can be opened using a key from the Key Traka cabinet. You will find all passwords (RAMSES, access screens, etc) in this blue box. 




