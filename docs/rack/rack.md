## Rack

### Rack Services
 Ensure that all the racks are powered ON and that the temperature of the racks are not exceeding the maximum limit.

The standard display is shown below. The panel is self-explanatory, read the instructions written on the panel: 

#### Rack Monitoring
Each table provides state and temperature of each rack in the corresponding experimental area.
 
- All racks should be **<span style="color: green;">ON</span>** (racks with no state are not controlled by DCS, should not be taken into account. Racks with no temperature information don't have a turbine);

- Double-click on a rack to obtain additional information;

- If a rack is **OFF** or in **ERROR** you should contact the rack's owner;

- If a rack is ON and goes to **OFF**, contact the rack's owner **and verify with DAQ shifter and the Shift Leader if they started seeing ERRORS** from the offending subdetector;

- To know the rack's owner, double-click on the rack, a window with all the details of the rack will appear.

- To know which crates belong to which subdetector, click [here](https://cmsonline.cern.ch/webcenter/portal/cmsonline?wc.contentSource=) (it will direct you to cmsonline web site) and login with your NICE account.

- List of states and colors:
	1. **<span style="color: green;">ON</span>**: rack is ON. This is the state it should be in; 
	2. **<span style="color: red;">ERROR</span>**: rack is in ERROR, requires human intervention. Contact the rack owner; 
	3. **<span style="color: blue;">OFF</span>**: rack is OFF. Contact the rack owner to verify if it is supposed to be OFF or needs to be turned back ON (see below for information on how to turn a rack ON); 
	4. **<span style="color: gray;">EXCLUDED</span>**: state of the particular rack is not accounted for; 
	5. **<span style="color: purple;">MASKED</span>**: rack temporarily masked, state will not be taken into account. 

#### Power Status Summary Table
- Indicates racks statistics.
- All numbers are clickable;
- By clicking on a number, this brings up a panel (as shown below). Double-click on the fields of this panel to obtain additional information on racks; 


#### How to turn ON a rack

In case you are asked by TC, subdetector experts or shift leader to send any commands to the CMS racks, please use
the Rack Wizard application (not the individual rack panel).

The wizard works by asking you questions concerning the operation which you need to do.

1. First question is “What do you need to do?”, and if you click on the field with the questions, you will see the possible answers from which you can choose. The most frequently used actions are “Switch on some whole racks” and “Switch off some whole racks”.

2. The next question is “Which racks are you interested in?”, and the most frequently used options are “Racks
belonging to a subdetector” and “Racks in a certain area”.

3. The last question depends on the answer to the previous question and allows you to choose the corresponding subdetector, the area or something else.

After you have answered all those questions you will have a pre-selection of racks in the left part.

To make the final selection, you need to move the necessary racks from the left part to the right. You can either move
the racks one by one with “>” button, or move all racks from the left part to the right by using “>>”.

To execute the chosen command on the selected racks, click on the “Execute” button in the top of the wizard. If you made a mistake, you can click “Cancel” and start the process of command and rack selection again.

After you clicked “executed” check that the commands on the racks were really executed, i.e. if you were asked to switch off some racks, check that the status changed to “OFF”.



The panel is self-explanatory. In order to turn a rack ON or send the Acknowledge command to clear an ERROR, read the instructions on the panel. Below is a summary on how to turn ON a rack. 

   * Use the Wizard under 'Rack Control' to change the state of a rack;
   * The Wizard will guide you to prepare the command to be sent;
   * When you finished preparing the command, find the summary under 'Command Summary';
    under 'Command Summary' double-check that the command to be sent to the rack is correct;
   * if the command to be sent is correct, move the rack from the table on the left (entitled 'Preliminary list of racks') to the one on the right (entitled 'Commands will only be sent to these racks') and click 'EXECUTE';
   * The command will not be sent until the 'EXECUTE' button is pressed. 

In case of more than 5 rack has problem, please follow the instruction for turn on multiple racks at **<span style="color: red;">Rack_Problems</span>** section 












