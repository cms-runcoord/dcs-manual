## Safety Tour

During the working days, the safety tour is performed by 2 CMS technicians 1 tour in the morning between 7h & 10h and 1 tour in the afternoon between 16h-19h. You can see the name of the persons in charge of the safety tour on the shiftlist tool under Safety tour. 

**The DCS shifter has to...**

* Do the safety tour during the night shift and the non working days. 

* Before going for the tour, the central DCS shifter has to inform the Shift Leader. 

The later will regularly check the central DCS status and contact the shifter in case of problem. The shift leader has to put his CERN badge on the DCS card reader. The Shift Leader will also have to open and then close the PM54 when the Safety Tourist has to access the Service Cavern. Instructions for opening and closing the PM54 access devices can be found in the How to close and open the PM54 access devices section.

Documentation about the Safety Tour can be found on the [Detector Safety - SLIMOS Tour](https://twiki.cern.ch/twiki/bin/view/CMS/OnlineWBSLIMOSInstructions) twiki page.

Please note: there is a noisy (clicking) pump in the magnet area. You can compare the noise you observe to the normal noise sample.

In case, you find something unusual which should be dealt with, please consult the [Safety FAQ](https://cmssafety.web.cern.ch/) and act accordingly.

**<span style="color: red;">Think twice before intervening on anything!</span>**

**Required Access**: CMS USC access.

**After the Safety Tour, please report anything abnormal in the SLIMOS elog.** 
