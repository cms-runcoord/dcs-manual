## Sensors4CMS

The **Sensors4CMS** groups geographically and makes available existing sensor data from CMS, publically available from different subdetectors or technical support groups.

![Sensor4cms](sensors4cms.png)

It is organized in regions of CMS and shows information on temperature and humidity.

It is to be used for informational purposes, which means it does not raise any alarms.

Detailed information about the **Sensors4CMS** can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/CMS/ProjectS4CMS). 




