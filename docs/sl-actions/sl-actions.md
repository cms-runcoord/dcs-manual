## Shift Leader Actions

**Following actions are taken by the shiftleader. Technical Shifter shall support him/her**


###DSS Local Emergency (e.g. subdetector) : Emergency Stop

In case a part of the detector or the underground needs to be switched off due to an emergency, take actions on the DSS panel (see picture below)

1. **Inject** the black key from the small blue box next to the DSS panel into the right side of the panel (see picture below). 

2. **Turn it** in order to enable actions on the panel and then **switch off** the corresponding part on the panel. (Some parts like the magnet are controlled with a separate key switch and/or might need a yellow key - both keys are in the blue key box next to the panel). 

3. Note that the water mist system is activated only if the key is injected in the corresponding switch on the left hand side of the panel, turned and if the corresponding fire sensor has triggered. The key alone will not activate the water mist system. 


 **Figure**: CMS Control room DSS panel and the key for enabling DSS actions:

![DCS Panel](panel.png)


### Global Fire or other Emergency in the Control Room: Full Emergency Stop 

Should CMS and the control room need to be evacuated during an emergency (e.g. fire): 

1. Issue the evacuation alarm! (see above)

2. **Inject** the black "Synoptique Full Shutdown" key from the small blue box next to the DSS panel into the Full Shutdown via DSS (see picture below) and **turn the key** in order to switch off the experiment.

3. Calmly leave the building.

 **Figure**: DSS action for switching off both caverns: 
![DSS shut down](dss-shutdown.png)



