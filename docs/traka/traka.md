## Traka Key Cabinet

The Traka key cabinet is used for distributing keys to personnel.

Depending on the task, a person will be assigned a certain key for a certain period of time.

After badging at the Traka Key, the available keys are marked by a green light.

In order to take a key, one needs to press the corresponding button and then pull out the key.

During your shift, you will have access to a number of keys.

If someone asks you to give him/her a key from the Traka Key cabinet:

- Ask the name of the person and the purpose of the key.
	- If the person is a guide whose visit is scheduled to start soon, requesting either the key number 12 or 44, you can issue the key without following further steps.
- Call TC on-call at 165000 and ask for derogation.
- Make a corresponding entry into the Elog. 


If a key is missing, you can find out the person who used it last:

- Press the * button.
- Enter the number of the key slot.
- Finalize by ##

In case of no possibility to get the keys due to power cut, network issues, etc.:

- Please call TC on-call 165000 (a key is available in one of the two red safes to open the Traka).
