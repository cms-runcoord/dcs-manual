## Underground Access Control 

### General Requirements 

   * Every person going underground (Service and Experimental caverns) is required to fill in the paper version of the 'Access to Experimental and Service Caverns' list that can be found in a binder by the Shift Leader's desk. 

   * A copy of this list can be found on [.pdf](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/Service_Cavern_Access_form-EDMS-1233310.pdf) and .docx versions. If you see that the empty paper sheets are about to finish, print on some more and put it in the binder. 

   * A copy of the sheet is also available on the Desktop of the Access Control machine. 
    

### How to close and open the PM54 access devices (turnstiles AND green PAD) 
    
In order to restrict unannounced access to the Service Cavern and ensure the safety rules, the access devices to USC55 cavern (turnstiles and green PAD) shall be **closed during the weekend and from 7 pm to 7 am every-night**, starting on the 20th of July 2011.

Instructions on how to close and open the PM54 access devices can be found here.


### First Cavern Access - Restricted (key)

When the cavern is closed the initial access procedure is very different from the later accesses. This section will contain instructions for this case. 

* **The main responsibility of the DCS Central Shifter is to call the Radiation Protection Expert (RPE) on the list below (in the order shown) and they will handle the remaining tasks.** 

* Before calling the RPE you should first check with the shift leader to confirm that a work package is in place. 

* The shift leader will confirm that the CCC has approved and scheduled the access. 

* The appropriate safety person (RPE) in CMS must be contacted. This person will be the first to enter the cavern, either for a radiation survey or to lead a group. The contacts are: 
    
    
| Contact List            |                  |
| ----------------------- | ---------------- |
| Maf Alidra              | 16 0040 (7 9854) |
| Sandro Di Vincenzo      | 16 4411 (7 8255) |
| Niels Dupont-Sagorin    | 16 5186 (7 1692) |
    
* Delegation Mode - If the cavern is to be opened for a significant period of time for multiple group accesses this mode is necessary. Call CCC and tell them you wish for CMS DELEGATION and need RESTRICTED mode for both access doors. (UP55 and UPX56) 

* Advance Preparations:

	1. A workpackage must be created showing the names of the persons needing access, job to be done, tools, etc. Access will not be permitted without this step. 

	2. Each person must be issued an extra radiation monitor. These are available in the red cabinet near the restrooms. The key is in the blue box near the DSS alarms panel. The badge number must be entered into the workpackage information. **The badge must be zeroed at this time.**

	3. If equipment will be taken into the Cavern the MAD key may be obtained from the safe opposite the Central-DCS consoles (floor level). **The Run Field Manager has access to the safe.**

	4. Future - In ~January Biocell training and equipment will be required to enter the cavern. 
     
* After the access:
	1. The exit time for each user must be entered by the Central-DCS shifter in the workpackage form. 

	2. The dosage must be read by the user and entered in the workpage by the Central-DCS shifter. Units are milli-Sieverts. 

	3. Close the workpackage form. It will move into the "Done" category. (It can still be edited if something was forgotten or incorrect.) 

	4. To close the cavern (no additional accesses), after all have exited and keys have been returned, call CCC and release the Delegation state. 



### Normal Cavern Access - General 

The access to the experimental cavern is only allowed to people that are listed on a valid [workpackage](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/ACT?_piref815_939262_815_939254_939254.strutsAction=%2FbrowseWpDb.do), i.e., the workpackage has to have either 'Ongoing' or 'Scheduled' status and have an up-to-date end date.

The check that a person is allowed to enter the experimental cavern is done automatically, when the person scans his/her ID card at the experiment entrance doors, UP55 and UPX56. In case it fails, the most common reasons are either that the person is not listed in any workpackage (in which case the person has to state the reason why he/she would like to access the experimental cavern and identify which workpackage this corresponds to) or the corresponding workpackage does not have the 'Ongoing' or 'Scheduled' status, or has an expired end date.

Further instructions on how to handle the cavern access can be found here.ppt or [here.pdf](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/DCScavernAccess1.3.pdf).

When the magnet is on, everybody entering UXC55 should be equipped with an active dosimeter (DMC). Instructions on how to use it can be found [here](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/DMCreaderManual.pdf).

The Technical shifter can not edit the workpackages (IMPACT). This can only be done by the workpackage (IMPACT) supervisor and Jim Cook or Lars Tore Roedne. Call 165000, when in doubt. 
    
    
### Normal Cavern Access - Restricted

In case the "restricted access" is in place, the DCS shifter is required to operate the access procedure. The terminal is located on the right side of the central DCS screens. It controls access only to the UXC cavern, not the USC cavern. **First find out who is allowed to go underground and for which reasons**. This information can be found in the [workpackage](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/ACT) page.

* The person willing to go underground has to be properly registered in a workpackage that:
	* has been approved by CMS technical coordination
	* has the 'Scheduled' or 'Ongoing' status
	* is up-to-date. 
        
* This check is done automatically when swiping the badge in the card reader at the experimental doors UP55 and UPX56.

* In case someone is not allowed in the experimental cavern, the DCS shifter has the rights to modify a workpackage. Any changes to a workpackage take about 20 minutes to be propagated to the appropriate database.

* In case of any doubts the DCS shifter should contact Martin Gastal 16 3702 or technical piquet 16 5000 for authorisation. 

![Underground screens](udgd1.png)

The left screen shows various surveillance cameras: entry and exit views for the UPX56 entrance, and for the UP55 entrance. There is also a camera inside of the material gate at UP55. **No person is ever allowed to pass through the material gate**. The video is recorded and can be played back at a later time using the controls at the bottom of the screen. 

The right screen shows the access controls either for UPX56 or UP55. Switch between UPX56 and UP55 by selecting "Vue Generale" on the bottom left. Then select "UPX56" or UP55.

   * During EYETS the procedure of taking the "Delegation" of UXC55 from CCC has been changed. The instructions on how to do it are available [here](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/DCS_Access_Screen_March2017.pdf). 

Audio connection to the entry and exit speakers of both entrances can also be established by clicking on the intercom icons (below the little camera window) OR by dialing the numbers directly on the intercom: Entry UP55: 145, Exit UP55: 146, Entry UPX56: 147, Exit UPX56: 148

**Emergency**: In case of emergency, the doors can be forced open from the inside by turning the lever (marked green). This always causes a beam dump... 


### Post Access Task - Cavern Light Check

The CMS Technical Shifter has the responsibility to check that at the end of each access the Light in UXC are properly turned OFF. The video cameras installed in UXC give sufficient feedback on the status of the lights. Furthermore the CMS technical Shifter should switch OFF the lights in case they are still ON at the end of an access (She/He can delegate this to any other person with USC access rights).

Instructions on how to switch OFF the UXC Lights can be found [here](https://twiki.cern.ch/twiki/pub/CMS/OnlineWBCentralDCSInstructions/HowToSwitchOffLightsInUXC.pdf).


### Guided group Visits

CMS welcomes groups of visitors on daily basis Monday to Saturday from 9:00 until 19:00. CMS members who are volunteering, guide the group on the surface as well as underground. All details regarding the visits organised can be found on the Visits Monitor screen: 

![Underground screens](undg3.png)

In case, a person/guide asks you for a token to take a visitor underground,without having made a visit booking before, please call 165000 to request an approval.

Guides should always follow the General Requirements

Rarely guides will need the Technical Shifter's help, as they will be using tokens to go underground. Tokens can replace the dosimeter when one wants to go underground in the service caveern USC55. The techincal shifter has to:

1. Verify that the visit is registered in the Visits Monitor
2. Bypass the biometry check for the guide/visitor who have tokens: you will have to look the camera & turn the button of the biometry 


### Access Controls Problem Guide

**Entry Failure**
   
   * If the door closed too soon or they were refused entry after entering the cabinet, have them reswipe their token.
   
   * If they are too close to the scanner then it will not focus properly. Have them move further or closer on the next try.
   
   * If they move during the scan it can fail. Exit and reswipe your token.
   
   * If the door bumps your foot as it is closing you must exit and re-try.
   
   * If it fails too many times, wait for the system to time out and then re-try.
   
   * If the entry door fails to close have them remove their helmet and try again.
   
   * If they are carrying a backpack, pc or other equipment they may have to use the MAD for these items. 

**Exit Failure**
   
   * If the light above the booth is not green, it will not allow you to exit.
   
   * If the user forgets to push the green button, it will not allow you to exit.
   
   * If the previous user has not yet returned their key the light above the booth door will not turn green.
   
   * Ask the user to exit, wait for the green light, scan their token, push the green button and enter the booth. 

**Key not returned**
   
   * Someone entered thru one access door and exited thru the other one. The key tray door will not open when they swipe their token!
   
   * Return to the original entry area. (Stay outside.)
   
   * Swipe your token/radiation badge. The key tray door will open.
   
   * Return the key. The door will close automatically.
   
   * Swiping your token at the correct location will cause the key tray to open. They may then return their key. 

**Some windows have disappeared**
   
   * Close all windows
   
   * Click on the key symbol labeled **multi-ecran** on the upper left of the right hand screen.
   
   * Log in.
   
   * Open the appropriate windows. 

**Computer problems (hang ups)**
   
   * Reboot the computer using the **START** menu (right screen, lower left corner).
   
   * When the login box appears, **CANCEL** it!!!
   
   * Click on the key symbol labeled **multi-ecran** on the upper left of the right hand screen.
    log in.
   
   * Wait until both screens have windows showing. The left screen will be the accelerator (LASS). The right screen will be the LACS screen.
   
   * On the left screen click on the **LACS** arrow.
   
   * On the left screen click on the **UP55** button.
   
   * On the right screen click on the non-interverouille button, then one of the **UPX54** buttons.
   
   * Check that the **CMS DELEGATION** button is yellow. USCmap.png 

**CMS DELEGATION Screen shows *FAULT**, Exploitation Access is **Off**.*
   
   * Call the TI Control room 7 2201
   
   * Request that they **reset the fault**
   
   * Request that they enable **CMS DELEGATION** and need **RESTRICTED** mode for both access doors (UP55 and UPX56).
   
   * In the right hand box at the lower right the grey square will become active. Click it and the CMS CAVERN access system will be in **DELEGATION mode**. 




















    
    
    
